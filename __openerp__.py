# -*- coding: utf-8 -*-
#################################################################################
#
#    Copyright (C) 2007-Today Odoo Indonesia (<http://odoo.id/>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
{
    'name': 'Indonesia Localization - State Name',
    'summary': "Indonesia's state",
    'version': '2.0',
    'author': 'Odoo Indonesia',
    'website': 'http://odoo.id',
    'category': 'Localization',
    'description': """
    """,
    'depends': ['base'],
    'data': ['data/res.country.state.csv'],
    'installable': True,
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
